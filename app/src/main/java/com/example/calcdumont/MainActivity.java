package com.example.calcdumont;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clear(View v) {
        EditText et1 = findViewById(R.id.edit_text_1);
        EditText et2 = findViewById(R.id.edit_text_2);
        TextView tv = findViewById(R.id.resultat);
        et1.setText("");
        et2.setText("");
        tv.setText(R.string.resultat);

    }

    public void compute(View v) {
        String et1 = ((EditText) findViewById(R.id.edit_text_1)).getText().toString();
        String et2 = ((EditText) findViewById(R.id.edit_text_2)).getText().toString();
        RadioGroup rg = findViewById(R.id.radio_group);
        if (et1.equals("") || et2.equals("") || et1.equals(".") || et2.equals(".")
                || rg.getCheckedRadioButtonId() == -1) {
            ((TextView) findViewById(R.id.resultat)).setText(R.string.erreur);
            return;
        }

        float result = 0;

        int id = rg.getCheckedRadioButtonId();
        if (id == R.id.radio_plus) {
            result = Float.parseFloat(et1) + Float.parseFloat(et2);
        } else if (id == R.id.radio_moins) {
            result = Float.parseFloat(et1) - Float.parseFloat(et2);
        } else if (id == R.id.radio_multiplie) {
            result = Float.parseFloat(et1) * Float.parseFloat(et2);
        } else if (id == R.id.radio_divise) {
            result = Float.parseFloat(et1) / Float.parseFloat(et2);
        }

        if (Float.isNaN(result) || Float.isInfinite(result)) {
            ((TextView) findViewById(R.id.resultat)).setText(R.string.erreur);
        } else {
            String res = Float.toString(result);
            if (res.endsWith(".0")) {
                res = res.substring(0, res.length() - 2);
            }
            ((TextView) findViewById(R.id.resultat)).setText(res);
        }

    }

    public void quit(View v) {
        finish();
    }

}